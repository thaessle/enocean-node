'use strict';

const { EventEmitter } = require('events');
const { promisify } = require('util');

const { EventEmitter: RustChannel } = require('./index');

// Supported actuator for now:
const SmartPlug = Object.freeze({
  Eep: 'd2010e',
  id: [0x05, 0x0a, 0x3d, 0x6a],
  commandList: {
    QueryPower: 'QueryPower',
    QueryEnergy: 'QueryEnergy',
    Off: 'Off',
    On: 'On',
  },
});

const EmergencyNecklet = Object.freeze({
  Eep: 'f60201',
  id: [0xfe, 0xf5, 0x8f, 0xf5],
  commandList: {},
});

// Javascript implementation of EventEmitter
class EnoceanCommunicator extends EventEmitter {
  constructor(sp) {
    super();
    this._shutdown = false;
    this.channel = new RustChannel(sp); // Channel between Rust and Node part
    const poll = promisify(this.channel.poll.bind(this.channel)); // Polling "hack"
    // Will be replaced when this PR will be merged : https://github.com/neon-bindings/neon/pull/375

      
    const loop = () => {
      if (this._shutdown) {
        return
      }
      
      poll()
        .then(({ event, ...data }) => this.emit(event, data))
        .catch(err => this.emit('error', err))
        .then(() => setImmediate(loop));
    };
    loop();
  }

  shutdown() {
		this._shutdown = true;
		return this;
	}
  
  send_enocean_cmd(eep, id, command) {
    this.channel.send_cmd(eep, id, command);
  }
}

function run() {
  // const serialport = '/dev/tty.usbserial-FT1ZKA73';
  const serialport = "/dev/tty.usbserial-FTWTOH0A";
  const emitter = new EnoceanCommunicator(serialport);
  // RECEIVED A MESSAGE FROM A KNOWN SENSOR/ACTUATOR
  emitter.on('esp3_erp1_received', ({ content }) =>
    console.log('Node received ESP3 Radio: ', content)
  );
  //RECEIVED A MESSAGE FROM USB300 (eg. after sending a command to an actuator. RC 0 = message valid)
  emitter.on('esp3_response_received', ({ content }) =>
    console.log('Node received ESP3 Response: ', content)
  );
  // Should not happen :
  emitter.on('esp3_unknown_received', ({ content }) =>
    console.log('Node received ESP3 unknown messsage: ', content)
  );

  // Should not happen :
  emitter.on('lib_error', ({ content }) =>
    console.log('MOTHERGUCKING LIB ERROR : ', content)
  );
  emitter.on('error', ({ content }) =>{

  // fonciton break loop
  console.log('ERREUR ICI ---------____---__-_---_-___', content);
  emitter.shutdown();
}
);

  // Command examples :
  // setTimeout(() => 	emitter.send_enocean_cmd(SmartPlug.Eep,SmartPlug.id,SmartPlug.commandList.Off) , 1000);
  // setTimeout(() => 	emitter.send_enocean_cmd(SmartPlug.Eep,SmartPlug.id,SmartPlug.commandList.On) , 4000);
  setTimeout(
    () =>
      emitter.send_enocean_cmd(
        SmartPlug.Eep,
        SmartPlug.id,
        SmartPlug.commandList.QueryPower
      ),
    2000
  );
  setTimeout(
    () =>
      emitter.send_enocean_cmd(
        SmartPlug.Eep,
        SmartPlug.id,
        SmartPlug.commandList.QueryEnergy
      ),
    3000
  );
}
module.exports = EventEmitter;
run();
