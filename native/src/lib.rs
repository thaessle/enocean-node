#[macro_use]
extern crate neon;
extern crate neon_serde;

extern crate serde_bytes;
extern crate serde_derive;
extern crate serde_json;

use std::sync::mpsc::{self, TryRecvError};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

use neon::context::{Context, FunctionContext, TaskContext};
use neon::object::Object;
use neon::result::{JsResult, JsResultExt};
use neon::task::Task;
use neon::types::{
	JsArray,
	JsFunction,
	// JsNumber,
	JsObject,
	JsString,
	JsUndefined,
	// JsValue,
};

use std::sync::mpsc::{Receiver, Sender};

extern crate enocean;
use enocean::eep::parse_erp1_payload;
use enocean::enocean::DataType;
use enocean::enocean::ESP3;
use neon::prelude::*;

pub enum Event {
	// Esp3_received { esp3: ESP3 },
	Esp3Received { esp3: ESP3 },
	ErrorReceived { error_str: String },
}

//https://github.com/neon-bindings/neon/issues/394
#[no_mangle]
pub extern fn __cxa_pure_virtual() {
    loop{};
}


// Event from RUST to NODE
fn event_thread(
	rx: mpsc::Receiver<String>,
	sp: String,
) -> mpsc::Receiver<Event> {
	let (tx, events) = mpsc::channel();
	thread::spawn(move || {
		// Enocean Stuff
		// Just to show how much enocean serial packets were received
		let mut nb_received = 0;
		// For now, this variable is hardcoded
		let port_name = sp.to_string(); //Get this from env?
		let (enocean_emiter, enocean_event_receiver) = mpsc::channel();
		let (enocean_command_receiver, enocean_commander) = mpsc::channel();

		// Create a thread to interact (both ways) with serial port
		// The interaction is achieved thanks to 2 channels (std::sync lib)
		let enocean_listener = thread::spawn(move || {
			enocean::communicator::listen(
				port_name,
				enocean_emiter,
				enocean_commander,
			);
		});

		loop {
			// ENOCEAN MESSAGE RECEIVED?
			match enocean_event_receiver.try_recv() {
				Ok(u) => {
					//u is the esp3 packet
					nb_received = nb_received + 1;
					println!("-- CPT : {}", nb_received);
					tx.send(Event::Esp3Received { esp3: u })
						.expect("Send failed");
				}
				Err(TryRecvError::Empty) => {}
				Err(e) => {
					// eprintln!("erreur POEJFPOIZEFJPZIOEFJ: {}", e);
					tx.send(Event::ErrorReceived{error_str:e.to_string()});
					break;
				}
			}
			match rx.try_recv() {
				Ok(cmd) => {
					// HARDCODED, TODO : treat EEP and ID en plus
					match cmd.as_ref() {
						"On" => {
							enocean_command_receiver.send(
								enocean::eep::create_smart_plug_command(
									[0x05, 0x0a, 0x3d, 0x6a],
									enocean::eep::D201CommandList::On,
								),
							);
						}
						"Off" => {
							enocean_command_receiver.send(
								enocean::eep::create_smart_plug_command(
									[0x05, 0x0a, 0x3d, 0x6a],
									enocean::eep::D201CommandList::Off,
								),
							);
						}
						"QueryEnergy" => {
							enocean_command_receiver.send(
								enocean::eep::create_smart_plug_command(
									[0x05, 0x0a, 0x3d, 0x6a],
									enocean::eep::D201CommandList::QueryEnergy,
								),
							);
						}
						"QueryPower" => {
							enocean_command_receiver.send(
								enocean::eep::create_smart_plug_command(
									[0x05, 0x0a, 0x3d, 0x6a],
									enocean::eep::D201CommandList::QueryPower,
								),
							);
						}
						_ => {}
					}
				}
				Err(TryRecvError::Disconnected) => {
					break;
				}
				Err(TryRecvError::Empty) => {}
			}
			thread::sleep(Duration::from_millis(10));
		}
	});
	events
}

pub struct EventEmitter {
	events: Arc<Mutex<mpsc::Receiver<Event>>>,
	send_cmd_chan: mpsc::Sender<String>,
	// serial_port : String
}

pub struct EventEmitterTask(Arc<Mutex<mpsc::Receiver<Event>>>);

impl Task for EventEmitterTask {
	// A Rust task that can be executed in a background thread.
	type Output = Event; // The task's result type, which is sent back to the main thread to communicate a successful result back to JavaScript.
	type Error = String; // The task's error type, which is sent back to the main thread to communicate a task failure back to JavaScript.
	type JsEvent = JsObject; //The type of JavaScript value that gets produced to the asynchronous callback on the main thread after the task is completed.

	///Perform the task, producing either a successful Output or an unsuccessful Error.
	///This method is executed in a background thread as part of libuv's built-in thread pool.
	fn perform(&self) -> Result<Self::Output, Self::Error> {
		let rx = self
			.0
			.lock()
			.map_err(|_| "Could not obtain lock on receiver".to_string())?;
		rx.recv()
			.map_err(|_| "Failed to receive event !".to_string())
	}

	///Convert the result of the task to a JavaScript value to be passed to the asynchronous callback.
	///This method is executed on the main thread at some point after the background task is completed.
	fn complete(
		self,
		mut cx: TaskContext,
		event: Result<Self::Output, Self::Error>,
	) -> JsResult<Self::JsEvent> {
		let event = event.or_else(|err| cx.throw_error(&err.to_string()))?;
		let esp3_js = cx.empty_object();
		let mut event_name = cx.string("");
		let mut object = JsObject::new(&mut cx);
		match event {
			// We received an event from RUST part
			Event::Esp3Received { esp3 } => match &esp3.data {
				DataType::Erp1Data {
					rorg,
					sender_id,
					status,
					payload,
				} => {
					let js_rorg = cx.number(*rorg as u8);
					object.set(&mut cx, "Rorg", js_rorg).unwrap();

					let js_status = cx.number(*status);
					object.set(&mut cx, "Status", js_status).unwrap();

					let js_array_sender: Handle<JsArray> = cx.empty_array();
					let js_sender_0 = cx.number(sender_id[0] as u8);
					let js_sender_1 = cx.number(sender_id[1] as u8);
					let js_sender_2 = cx.number(sender_id[2] as u8);
					let js_sender_3 = cx.number(sender_id[3] as u8);
					js_array_sender.set(&mut cx, 0, js_sender_0)?;
					js_array_sender.set(&mut cx, 1, js_sender_1)?;
					js_array_sender.set(&mut cx, 2, js_sender_2)?;
					js_array_sender.set(&mut cx, 3, js_sender_3)?;
					object.set(&mut cx, "Sender", js_array_sender).unwrap();

					let parsed = parse_erp1_payload(&esp3);
					let parsed_js: Handle<JsArray> = cx.empty_array();
					match parsed {
						Ok(parsed_packet) => {
							for (key, value) in parsed_packet.iter() {
								let js_parsed_key = cx.string(&key);
								let js_parsed_value = cx.string(&value);
								parsed_js.set(
									&mut cx,
									js_parsed_key,
									js_parsed_value,
								)?;
							}
							object.set(&mut cx, "Parsed", parsed_js).unwrap();
						}
						Err(e) => {
							eprintln!("Error :{}  ", e);
						}
						_ => {}
					};
					event_name = cx.string("esp3_erp1_received");
				}
				DataType::ResponseData {
					return_code,
					response_payload,
				} => {
					println!(
						"Response from TCM300 with RC : {:X?}",
						*return_code as u8
					);
					match response_payload {
						Some(ref payload) => {
							println!("And Payload: {:X?}", payload);
						}
						None => {}
					}
					let js_rc = cx.number(*return_code as u8);
					object.set(&mut cx, "ReturnCode", js_rc).unwrap();
					event_name = cx.string("esp3_response_received");
				}
				DataType::RawData { raw_data } => {
					println!("Unknow message: {:X?}", raw_data);
					event_name = cx.string("esp3_unknown_received");
				}
			},
			Event::ErrorReceived { error_str } => {
				// println! {"ERREUR DE NOTRE SUPERBE ADDON NODE {}", error_str};
				event_name = cx.string("lib_error");
				let error_string = cx.string(&error_str);
				object.set(&mut cx, "message", error_string).unwrap();
			},
		}
		esp3_js.set(&mut cx, "event", event_name)?;
		esp3_js.set(&mut cx, "content", object)?;
		Ok(esp3_js)
	}
}

declare_types! {
	pub class JsEventEmitter for EventEmitter {
		init(mut cx) {
			let serial_port = cx.argument::<JsString>(0)?;
			let str_serial_port = serial_port
				.downcast::<JsString>()
				.or_throw(&mut cx)?
				.value();
			let (send_cmd_chan, shutdown_rx):(Sender<String>, Receiver<(String)>) = mpsc::channel();
			let rx = event_thread(shutdown_rx, str_serial_port);

			Ok(EventEmitter {
				events: Arc::new(Mutex::new(rx)),
				send_cmd_chan,
			})
		}
		method poll(mut cx) {
			let cb = cx.argument::<JsFunction>(0)?;
			let this = cx.this();
			let events = cx.borrow(&this, |emitter| Arc::clone(&emitter.events));
			let emitter = EventEmitterTask(events);

			emitter.schedule(cb);
			Ok(JsUndefined::new().upcast())
		}

		method send_cmd(mut cx) {
			let this = cx.this();
			let eep = cx.argument::<JsString>(0)?;
			let _str_eep = eep
			.downcast::<JsString>()
			.or_throw(&mut cx)?
			.value();
			// println!("eep: {}", str_eep);

			let id = cx.argument::<JsArray>(1)?;
			let _array_id = id
			.downcast::<JsArray>()
			.or_throw(&mut cx)?;
			// println!("arrayLen: {}", array_id.len());

			let cmd = cx.argument::<JsString>(2)?;
			let str_cmd = cmd
			.downcast::<JsString>()
			.or_throw(&mut cx)?
			.value();

			cx.borrow(&this, |emitter| emitter.send_cmd_chan.send( str_cmd.to_string() ))
				.or_else(|err| cx.throw_error(&err.to_string()))?;
			Ok(JsUndefined::new().upcast())
		}
	}
}

register_module!(mut cx, {
	cx.export_class::<JsEventEmitter>("EventEmitter")?;
	Ok(())
});
