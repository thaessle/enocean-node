# neon-enocean   
   
:warning: UNDER CONSTRUCTION :warning:  

This lib is a Rust enocean lib binded to JS thanks to [Neon](https://github.com/neon-bindings/neon)
The rust lib (in alpha stage for now) is hosted :
- On  [crates.io](https://crates.io/crates/enocean) to be easily imported into rust program.
- On [Cutii github account](https://github.com/Cutii/enocean) for version management (...)
  
Usage :    
================  

**Requirements** :  
- Node [link to nvm install](https://github.com/creationix/nvm#installation)   
- Rustc / Cargo [link to the cargo book](https://doc.rust-lang.org/cargo/getting-started/installation.html)   
- Neon-bindings [link to neon git](https://github.com/neon-bindings/neon)   
 
**Usage** :   
```console   
neon build
node src/enocean.js   
```
**File structure** :  
`native/src/lib.rs` : rust bindings  
`src/enocean.js` : JS source code using rust bindings  

Functionnalities overview :
================

**src/enocean.js**  
1- Create an "enocean communicator" : 

```javascript
    var serialport = "/dev/tty.usbserial-FT1ZKA73";
	const emitter = new EnoceanCommunicator(serialport);
```
2- Receive events : 

```javascript
    // RECEIVED A MESSAGE FROM A KNOWN SENSOR/ACTUATOR
	emitter.on('esp3_erp1_received', ({ content }) => console.log('Node received ESP3 Radio: ', content));
	//RECEIVED A MESSAGE FROM USB300 (eg. after sending a command to an actuator. RC 0 = message valid)
	emitter.on('esp3_response_received', ({ content }) => console.log('Node received ESP3 Response: ', content));
	// Should not happen :
	emitter.on('esp3_unknown_received', ({ content }) => console.log('Node received ESP3 unknown messsage: ', content));
```
3- Send commands :  
* Supported actuator / commands "enumeration" :
```javascript
// Supported actuator for now : the only encoean smartplug we have :
var SmartPlug= Object.freeze(
	{
		"Eep":"d2010e",
		"id":[0x05, 0x0a, 0x3d, 0x6a],
		"commandList":{
			QueryPower:"QueryPower",
			QueryEnergy:"QueryEnergy",
			Off:"Off",
			On:"On"
		}
	});

```
* Send command :   
```javascript
    // Prototype : send_enocean_cmd(eep,id,command). Example : send command to power up the smartplug :
    emitter.send_enocean_cmd(SmartPlug.Eep,SmartPlug.id,SmartPlug.commandList.On);
```

Details on received messages from sensors : 
================  

Cutii usecase for now :   
  * SmartPlug measurment response for both POWER and ENERGY :  
  *This message is sent by an actuator if one of the following events occurs: Measurement results trigger an automated transmission (see Actuator Set Measurement message) OR Message Actuator Measurement Query has been received*  

  * SmartPlug status update  :  
  *This message is sent by an actuator if one of the following events occurs: Status of one channel has been changed locally OR Message Actuator Status Query has been received*

  * Temperature sensor message 

  * Pushbutton (Necklace and soft remote)  

  
**Associated EEPs:**  
	A50401 : Temperature Humidity sensor   
	D2010E :  SmartPlug  
	D50001 :  DoorContact   
	F60201 :  Pushbutton (Necklace)  
	F60202 :  Pushbutton (Soft Remote)  

Extract of EEP v2.6.7 from EnOcean : 

* SmartPlug measurment response : 
![](assets/img/EEP_measurmentResponse.png)  

* SmartPLug status update :
![](assets/img/EEP_statusUpdate.png)  

* Temperature sensor message :
![](assets/img/EEP_A50205.jpg)  

* necklace 

**Associated Cutii-enocean-node messages:**  
```javascript  
// Message from Pushbutton (softremote)
{ 
Rorg: 1,
Status: 32,
Sender: [ 0, 49, 192, 249 ],
Parsed: 
  [ SA: 'No 2nd action', EB: 'Released', R2: 'A1', R1: 'A1' ]
}
```
```javascript  
// Message from SmartPLug (Mesurement response)
{ 
	Rorg: 210, // 0d210 = 0xD2
  	Status: 0,
  	Sender: [ 5, 10, 61, 106 ],
	Parsed: [ 'I/O': '0', MV: '3', UN: 'Power[W]' ]  // Le channel 0 (un seul channel pour nous) consomme actuellement 3 watts
}
```
```javascript  
// Message from Temperature sensor 
{ 
  Rorg: 165, // 0xA5
  Status: 0,
  Sender: [ 5, 17, 114, 247 ],
  Parsed:
   [ TMP: '22.88',
     TSN: 'Temperature sensor available',
     HUM: '33.600002', LRNB: 'Data telegram' ] 
	 // Température actuelle : 22,8°C, humidité relative : 33,6 %RH
}
```


To be continued ...



